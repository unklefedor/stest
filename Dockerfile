FROM golang
ADD . /go/src/bitbucket.org/unklefedor/stest
WORKDIR /go/src/bitbucket.org/unklefedor/stest
RUN cd cmd/http_api && go build
ENTRYPOINT cd cmd/http_api && ./http_api --config=../../local.yml --db=../../db/positions.db

EXPOSE 8080