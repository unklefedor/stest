package db

import (
	"bitbucket.org/unklefedor/stest/config"
	"bitbucket.org/unklefedor/stest/db/models"
	"database/sql"
	"fmt"
	"os"

	// sql driver
	_ "github.com/mattn/go-sqlite3"
)

// DB - database interface
type DB interface {
	models.Queries
	Connect() error

	Close()
}

type db struct {
	connection	*sql.DB
	cfg			config.DBConfig

	*models.Managers
}

// New - return new db instance
func New(cfg config.DBConfig) DB {
	return &db{
		cfg: cfg,
	}
}

// Connect - connect to db
func (db *db) Connect() error {
	if db.connection != nil {
		db.connection.Close()
	}

	// Check for file exists
	if _, err := os.Stat(fmt.Sprintf("%s", db.cfg.Path)); os.IsNotExist(err) {
		return err
	}

	sqlDb, err := sql.Open("sqlite3", fmt.Sprintf("%s", db.cfg.Path))
	if err != nil {
		return err
	}

	if err := sqlDb.Ping(); err != nil {
		return err
	}

	db.connection = sqlDb
	db.Managers = models.NewManager(db.connection)

	return nil
}

// Close - close connection to db
func (db *db) Close() {
	db.connection.Close()
}
