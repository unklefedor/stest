package models

import (
	"database/sql"
	"time"
)

// Model - standard db table model
type Model struct {
	Updated time.Time `json:"-"`
}

// SQLConnection - sql connection interface
type SQLConnection interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
	Exec(query string, args ...interface{}) (sql.Result, error)
}

// Queries - queries of all models
type Queries interface {
	PositionQueries
}

// Managers - manager of all models
type Managers struct {
	*PositionManager
}

// NewManager - return new manager instance of all models
func NewManager(connection SQLConnection) *Managers {
	return &Managers{
		&PositionManager{connection},
	}
}
