package models

import (
	"database/sql"
	"errors"
	"fmt"
	"regexp"
)

const pageSize = 100

// PositionsRow - representation of positions table
type PositionRow struct {
	Model
	Keyword		string
	Position	int
	Url			string
	Volume		int
	Results		int
}

// PositionQueries - represents allowed position queries
type PositionQueries interface {
	GetSummaryByDomain(domain string) (int, error)
	GetPositionsByParams(domain string, sort string, page int) ([]*PositionRow, error)
}

// PositionManager - currencies model manager
type PositionManager struct {
	connection SQLConnection
}

// GetPositionsByParams - return positions by selected domain, page && sort are optional
func (m *PositionManager) GetPositionsByParams(domain string, sort string, page int) ([]*PositionRow, error) {
	// Base
	sql := "SELECT keyword, position, url, volume, results, updated FROM positions WHERE domain = ?"

	// SQL Driver cannot set placeholder parameters as ORDER BY section, so i must do a flip
	// But i must check param for SQL injection
	valid := regexp.MustCompile("^[A-Za-z0-9_]+$")
	if !valid.MatchString(sort) {
		return nil, errors.New("unsafe sort param, injection probability")
	}
	sql = fmt.Sprintf("%s ORDER BY %s DESC", sql, sort)

	// Adding pagination section
	if page > 0 {
		sql = fmt.Sprintf("%s %s", sql, "LIMIT ? OFFSET ?")
	}

	rows, err := m.connection.Query(sql, /* domain */ domain, /* LIMIT */ pageSize, /* OFFSET */ (page - 1) * pageSize)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	return m.parseMultiplyRows(rows)
}

// GetSummaryByDomain - return position summary by specified domain
func (m *PositionManager) GetSummaryByDomain(domain string) (int, error) {
	var c int

	rows, err := m.connection.Query("SELECT count(*) as c FROM positions WHERE domain = ?", domain)
	if err != nil {
		return 0, err
	}

	defer rows.Close()

	if !rows.Next() {
		return 0, nil
	}

	return c, rows.Scan(&c)
}

func (m *PositionManager) parseMultiplyRows(rows *sql.Rows) ([]*PositionRow, error) {
	positions := make([]*PositionRow, 0)

	for rows.Next() {
		p := &PositionRow{}
		if err := rows.Scan(&p.Keyword, &p.Position, &p.Url, &p.Volume, &p.Results, &p.Updated); err != nil {
			return positions, err
		}

		positions = append(positions, p)
	}

	return positions, rows.Err()
}
