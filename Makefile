API_NAME 			:= http_api

VERSION        		:= $(shell git describe --tags --always --dirty="-dev")
DATE           		:= $(shell date -u '+%Y-%m-%d-%H:%M UTC')
VERSION_FLAGS  		:= -ldflags='-X "main.Version=$(VERSION)" -X "main.BuildTime=$(DATE)"'
PKG            		:= bitbucket.org/unklefedor/stest
PKG_LIST       		:= $(shell go list ${PKG}/... | grep -v /vendor/)
CONFIG         		:= $(wildcard local.yml)
NAMESPACE	   		:= "default"

all: setup test build

setup: ## Installing all service dependencies.
	echo "Setup..."
	GO111MODULE=on go mod vendor

cfg: ## Creating the local config yml.
	@echo "Creating local config yml ..."
	cp config.example.yml local.yml

build: ## Build the executable http api file of service.
	@echo "Building..."
	cd cmd/$(API_NAME) && go build

run: build ## Run service http api with local config.
	@echo "Running..."
	cd cmd/$(API_NAME) && ./$(API_NAME) -config=../../local.yml -db=../../db/positions.db

lint: ## Run lint for all packages.
	echo "Linting..."
	GO111MODULE=off go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

	golangci-lint run --enable-all --disable gochecknoglobals \
	--disable lll -e "weak cryptographic primitive" -e InsecureSkipVerify \
	-e "G201: SQL string formatting" --disable dupl --print-issued-lines=false

lint\:config: ## Add pre-commit hook to git.
	echo "Pre-commit hook setup..."
	cp .git/hooks/pre-commit.sample .git/hooks/pre-commit
	sed -ie '$$s/^/#/' .git/hooks/pre-commit
	echo "make lint" >> .git/hooks/pre-commit

test: ## Run tests for all packages.
	@echo "Testing..."
	go test -race ${PKG_LIST}

coverage: ## Calculating code test coverage.
	@echo "Calculating coverage..."
	PKG=$(PKG) ./tools/coverage.sh

clean: ## Cleans the temp files and etc.
	@echo "Clean..."
	rm -f cmd/$(MIGRATE_NAME)/$(MIGRATE_NAME)
	rm -f cmd/$(API_NAME)/$(API_NAME)
	rm -f cmd/$(SAVER_NAME)/$(SAVER_NAME)

help: ## Display this help screen
	@grep -E '^[a-zA-Z_\-\:]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {gsub(/[\\]*/,""); printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
