package main

import (
	"bitbucket.org/unklefedor/stest/config"
	"bitbucket.org/unklefedor/stest/db"
	"bitbucket.org/unklefedor/stest/http"
	"flag"
	"fmt"

	"github.com/lillilli/logger"
	"github.com/lillilli/vconf"
	"github.com/pkg/errors"

	"os"
	"os/signal"
	"syscall"
)

var (
	configFile = flag.String("config", "", "set service config file")
	dbFile = flag.String("db", "", "set db filePath")
)

func main() {
	flag.Parse()

	cfg := &config.Config{}
	if err := vconf.InitFromFile(*configFile, cfg); err != nil {
		fmt.Printf("unable to load config: %s\n", err)
		os.Exit(1)
	}

	logger.Init(cfg.Log)
	log := logger.NewLogger("positions api")

	if *dbFile == "" {
		log.Error("DB flag is required")
		os.Exit(1)
	}

	db := db.New(config.DBConfig{Path: *dbFile,})
	if err := db.Connect(); err != nil {
		log.Errorf("DB connecting failed: %v", err)
		os.Exit(1)
	}

	defer db.Close()

	if err := startHTTPServer(cfg.HTTP, db, log); err != nil {
		log.Errorf("Start http server failed: %v", err)
	}
}

func startHTTPServer(cfg config.HTTPServer, db db.DB, log logger.Logger) error {
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	server := http.NewServer(cfg, db, log)
	if err := server.Start(); err != nil {
		return errors.Wrap(err, "unable to start http server")
	}

	defer server.Stop()

	<-signals
	close(signals)

	return nil
}
