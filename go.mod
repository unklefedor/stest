module bitbucket.org/unklefedor/stest

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/lillilli/logger v0.0.0-20190312093536-8f249b316b4d
	github.com/lillilli/vconf v0.0.0-20190403152641-ca1e45112696
	github.com/mattn/go-sqlite3 v2.0.1+incompatible
	github.com/pkg/errors v0.8.1
)
