package http

import (
	"bitbucket.org/unklefedor/stest/config"
	"bitbucket.org/unklefedor/stest/db"
	"bitbucket.org/unklefedor/stest/http/handler/position"
	"context"
	"fmt"
	"github.com/lillilli/logger"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

const (
	readTimeout  = time.Minute
	writeTimeout = readTimeout
)

// Server - http server interface
type Server interface {
	Start() error
	Stop() error
}

// server - http sever structure
type server struct {
	server 	*http.Server
	mux    	*mux.Router

	db		db.DB

	log		logger.Logger
}

// NewServer - return new instance of http server
func NewServer(cfg config.HTTPServer, db db.DB, log logger.Logger) Server {

	api := &server{
		mux: mux.NewRouter(),
		db:  db,

		log: log,
	}

	api.server = &http.Server{
		Addr:         fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
		Handler:      api.mux,
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
	}

	return api
}

// Start - start http server
func (s server) Start() error {
	s.log.Info("[INFO] http server: Starting...")

	s.declareRoutes()

	tcpAddr, err := net.ResolveTCPAddr("tcp4", s.server.Addr)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("unable to get address %s", s.server.Addr))
	}

	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("unable to start listening %s", tcpAddr))
	}

	go func() {
		err := s.server.Serve(listener)
		if err != nil {
			s.log.Errorf("unable to start serve %v", err)
		}
	}()

	s.log.Infof("[INFO] http server: Listening on %s", s.server.Addr)
	return nil
}

func (s server) declareRoutes() {
	apiRouter := s.mux.PathPrefix("/api/").Subrouter()
	apiRouter.HandleFunc("/health", HealthCheckHandler).Methods(http.MethodGet)

	positionHandler := position.New(s.log, s.db)
	apiRouter.HandleFunc("/summary", positionHandler.Summary).Methods(http.MethodGet)
	apiRouter.HandleFunc("/positions", positionHandler.Positions).Methods(http.MethodGet)
}

// Stop - shutdown http server
func (s server) Stop() error {
	s.log.Info("[INFO] http server: Stopping...")

	err := s.server.Shutdown(context.Background())
	return err
}