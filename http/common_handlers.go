package http

import (
	"github.com/lillilli/logger"
	"strconv"
	"time"

	"net/http"
)

// HealthCheckHandler - represents http server status
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	t := strconv.Itoa(int(time.Now().Unix()))

	_, err := w.Write([]byte(t))
	if err != nil {
		log := logger.NewLogger("health checker handler")
		log.Errorf("Error sending response, HealthCheckHandler: %v", err)
	}
}