package position

import (
	"bitbucket.org/unklefedor/stest/db"
	"bitbucket.org/unklefedor/stest/http/handler"
	"github.com/lillilli/logger"
	"net/http"
)

type Handler struct {
	*handler.BaseHandler

	db	db.DB

	log logger.Logger
}

// New - create a new summary handler
func New(log logger.Logger, db db.DB) *Handler {
	return &Handler{
		BaseHandler: &handler.BaseHandler{Log:log},
		db:          db,
		log:         log,
	}
}

// Summary - handle summary request
func (h *Handler) Summary(w http.ResponseWriter, r *http.Request) {
	params, err := parseParams(r)
	if err != nil {
		h.SendBadRequestError(w, err.Error())

		return
	}

	h.handleSummaryRequest(w, params)
}

// Positions - handle positions request
func (h *Handler) Positions(w http.ResponseWriter, r *http.Request) {
	params, err := parseParams(r)
	if err != nil {
		h.SendBadRequestError(w, err.Error())

		return
	}

	h.handlePositionsRequest(w, params)
}

// handleSummaryRequest - summary response
func (h *Handler) handleSummaryRequest(w http.ResponseWriter, params *positionParams) {
	p, err := h.db.GetSummaryByDomain(params.Domain)
	if err != nil {
		h.SendBadRequestError(w, err.Error())

		return
	}

	response := &SummaryResponse{
		Domain:	params.Domain,
		PostionsCount: p,
	}

	h.SendMarshalResponse(w, response)
}

// handlePositionsRequest - positions by domain
func (h *Handler) handlePositionsRequest(w http.ResponseWriter, params *positionParams) {
	p, err := h.db.GetPositionsByParams(params.Domain, params.Sort, params.Page)
	if err != nil {
		h.SendBadRequestError(w, err.Error())

		return
	}

	response := &PositionResponse{
		Domain: params.Domain,
		Positions: make([]*Position, 0),
	}
	for _, v := range p {
		response.Positions = append(response.Positions, &Position{
			Keyword:  v.Keyword,
			Position: v.Position,
			Url:      v.Url,
			Volume:   v.Volume,
			Results:  v.Results,
			Updated:  v.Updated,
		})
	}

	h.SendMarshalResponse(w, response)
}