package position

import (
	"errors"
	"net/http"
	"strconv"
	"strings"
)

type positionParams struct {
	Domain	string
	Page	int
	Sort	string
}

func parseParams(r *http.Request) (*positionParams, error) {
	reqParams := &positionParams{}

	reqParams.Domain = strings.ToLower(r.URL.Query().Get("domain"))
	if reqParams.Domain == "" {
		return nil, errors.New("domain parameter is required")
	}

	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		page = -1
	}
	reqParams.Page = page

	sort := strings.ToLower(r.URL.Query().Get("sort"))
	if sort == "" {
		sort = "volume"
	}
	reqParams.Sort = sort

	return reqParams, nil
}
