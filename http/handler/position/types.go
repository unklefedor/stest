package position

import "time"

type SummaryResponse struct {
	Domain			string	`json:"domain"`
	PostionsCount	int		`json:"positions_count"`
}

type PositionResponse struct {
	Domain		string	`jasn:"domain"`
	Positions	[]*Position	`json:"positions"`
}

type Position struct {
	Keyword		string		`json:"keyword"`
	Position	int			`json:"position"`
	Url			string		`json:"url"`
	Volume		int			`json:"volume"`
	Results		int			`json:"results"`
	Updated		time.Time	`json:"updated"`
}